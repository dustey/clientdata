# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-05-22 18:02-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Language-Team: Italian (https://www.transifex.com/akaras/teams/959/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. (itstool) path: texts/text
#: texts.xml:6
msgid "Hello"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:7
msgid "Goodbye"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:8
msgid "Thank you"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:9
msgid "Good job"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:10 texts.xml:16
msgid "I need help"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:11
msgid "Heal me"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:12
msgid "Follow me"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:13
msgid "Stop"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:14
msgid "Watch Out"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:15
msgid "Go away"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:17
msgid "Speak to this NPC"
msgstr ""
